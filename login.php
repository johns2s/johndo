<?php

  include("header.php");

  if (!isset($_SESSION["user"])) {

?>

<section class = "bigHeader">
  <section class = "bigHeader__content">
    <h1>Login</h1>
    <?php
      if (isset($_GET["message"])) {
        echo "
        <section class = 'bigHeader__content__more'>
          <section class = 'bigHeader__content__message'>
            <span class='material-icons'>error_outline</span>
            <span class = 'bigHeader__content__subtitle'>" . htmlentities($_GET['message']) . "</span>
          </section>
        </section>";
      }
      else {
        echo "
          <section class = 'bigHeader__content__more'>
            <span class = 'bigHeader__content__subtitle'>Not registered yet? Signup <a href = 'signup.php'>here</a> for free.</span>
          </section>
        ";
      }
    ?>

  </section>
</section>

<section class = "logsignWrap">
  <form class = "item" action = "logsign.php" method = "POST">
    <label class = "formLabel">Email Address</label>
    <input type = "email" class = "inputText" name = "emailInput" placeholder = "Email Address" required>
    <label class = "formLabel">Password</label>
    <input type = "password" class = "inputText" name = "passwordInput" placeholder = "Password" required>
    <input type = "submit" class = "inputBtn" name = "submitLogin" value = "Login" required>
  </form>
</section>

<?php

  include("footer.php");

  }

  else {
    header("location: index.php");
  }

?>
