<?php

include("header.php");

if (isset($_SESSION["user"])) {
  if (isset($_GET["view"]) && ($_GET["view"] == "everything" || $_GET["view"] == "today")) {
    $view = $_GET["view"];
  }
  else {
    $userID = $_SESSION["userID"];

    $sqlView = "SELECT view FROM settings WHERE user = '$userID' LIMIT 1";
    $resultView = mysqli_query($conn, $sqlView);
    if (mysqli_num_rows($resultView) == 1) {
      while($rowView = mysqli_fetch_assoc($resultView)) {
        $view = $rowView["view"];
      }
    }
    else {
      $view = "everything";
    }
  }

  ?>
    <!-- Include new task shortcut in both views -->
    <script src="https://cdn.jsdelivr.net/npm/mousetrap@1/mousetrap.min.js"></script>
    <script>
      Mousetrap.bind('t', function() {
        window.location = "newtask.php";
      });
    </script>
  <?php

  if ($view == "today") {
    include("today.php");
  }

  else {
    include("loggedin.php");
  }

}
else {
  include("loggedout.php");
}

include("footer.php");

?>
