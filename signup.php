<?php

  include("header.php");

  if (!isset($_SESSION["user"])) {

?>

  <script type = "text/javascript" async>
    function validatePassword() {
      var password = document.getElementById("password");
      var confirmPassword = document.getElementById("confirmPassword");
      if (password.value != confirmPassword.value) {
        confirmPassword.setCustomValidity("Passwords don't match");
      }
      else {
        confirmPassword.setCustomValidity("");
      }
    }
  </script>

<section class = "bigHeader">
  <section class = "bigHeader__content">
    <h1>Signup</h1>
    <?php
      if (isset($_GET["message"])) {
        echo "
        <section class = 'bigHeader__content__more'>
          <section class = 'bigHeader__content__message'>
            <span class='material-icons'>error_outline</span>
            <span class = 'bigHeader__content__subtitle'>" . htmlentities($_GET['message']) . "</span>
          </section>
        </section>";
      }
      else {
        echo "
          <section class = 'bigHeader__content__more'>
            <span class = 'bigHeader__content__subtitle'>Registered already? Login <a href = 'login.php'>here</a>.</span>
          </section>
        ";
      }
    ?>

  </section>
</section>

<section class = "logsignWrap">
  <form class = "item" action = "logsign.php" method = "POST">
    <label class = "formLabel">Email Address</label>
    <input type = "email" class = "inputText" name = "emailInput" placeholder = "Email Address" required>
    <label class = "formLabel">Password</label>
    <input id = "password" type = "password" class = "inputText" name = "passwordInput" placeholder = "Password" onchange = "validatePassword()" minlength="8" required>
    <label class = "formLabel">Confirm Password</label>
    <input id = "confirmPassword" type = "password" class = "inputText" name = "confirmPasswordInput" placeholder = "Confirm Password" onkeyup = "validatePassword()" minlength="8" required>
    <div class = "sub">
      <div style = "display: grid; grid-template-columns: auto 1fr; align-items: center; grid-gap: 15px; margin-bottom: 25px;">
        <img src = "static/warning.avif" style = "width: 32px;">
        <h3 style = "margin: 0; text-align: left;">Do not trust johnDo.</h3>
      </div>
      <ul style = "margin: 0 0 10px 15px; text-align: left;">
        <li>johnDo isn't production-ready and you may experience <b> data loss or theft (hacking)</b></li>
        <li>Do not use johnDo for personal or sensitive information</li>
        <li><b>Do not use your johnDo password anywhere else</b></li>
        <li>Do not use johnDo for critical information</li>
        <li>johnDo is offered without charge or warranty (under the Mozilla Public License 2.0) in the hope it may prove useful for people willing to take these risks</li>
      </ul>
      <p style = "margin-bottom: 15px">johnDo works best with Chrome or Firefox. We highly recommend you use a password manager like 1Password or Bitwarden.</p>
    </div>
    <input type = "submit" class = "inputBtn" name = "submitSignup" value = "Signup" required>
  </form>
</section>

<?php

  include("footer.php");

  }

  else {
    header("location: index.php");
  }

?>
