<?php

  include("header.php");

  if (isset($_SESSION["user"])) {

    if (isset($_POST["submitNewTitle"])) {
      $taskTitle = $_POST["titleInput"];
    }

    else {
      $taskTitle = "";
    }

    $userID = $_SESSION["userID"];
    $sqlSet = "SELECT defaultStartDate FROM settings WHERE user = '$userID' LIMIT 1";
    $resultSet = mysqli_query($conn, $sqlSet);
    if (mysqli_num_rows($resultSet) == 1) {
      while($rowSet = mysqli_fetch_assoc($resultSet)) {
        $defaultStartDate = $rowSet["defaultStartDate"];
      }
    }

?>

<script src="https://cdn.tiny.cloud/1/2og7s3xdcmvdj7cc6q76gpmwvvybp8b9scchl4ag1l3oerel/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#moreInput',
    toolbar_mode: 'floating',
    toolbar: 'undo redo | h1 | bold italic link | bullist numlist | outdent indent | insertDoneIcon',
    plugins: 'autolink autoresize lists advlist link charmap',
    menubar: false,
    contextmenu: false,
    skin: (window.matchMedia("(prefers-color-scheme: dark)").matches ? "oxide-dark" : ""),
    content_css: (window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : ""),
    init_instance_callback: function (editor) {
      editor.shortcuts.add(
        'meta+13', "Submit form", function () {
          document.querySelector(".inputTextBig").form.requestSubmit(document.querySelector("#subBtn"));
      });
      editor.shortcuts.add(
        'meta+d', "Insert done icon", function () {
          editor.insertContent('✅ ');
      });
    },
    setup: function (editor) {
      editor.ui.registry.addButton('insertDoneIcon', {
        text: '✅',
        tooltip: 'Insert done icon',
        onAction: function (_) {
          editor.insertContent('✅');
        }
      });
    }
  });
</script>

<section class = "bigHeader">
  <section class = "bigHeader__content">
    <h1>Create a task</h1>
    <section class = "bigHeader__content__more">
      <span class = "bigHeader__content__subtitle">It's <?php echo date("l, F jS"); ?>.</span>
    </section>
    <?php
      if (isset($_GET["message"])) {
        echo "
        <section class = 'bigHeader__content__more'>
          <section class = 'bigHeader__content__message'>
            <span class='material-icons'>error_outline</span>
            <span class = 'bigHeader__content__subtitle'>" . htmlentities($_GET['message']) . "</span>
          </section>
        </section>";
      }
    ?>

  </section>
</section>

  <section class = "editorWrap">
    <form class = "item editor" action = "process.php" method = "POST">
      <section id = "editor__main">
        <label class = "formLabel">Task Name</label>
        <input type = "text" class = "inputText" name = "titleInput" maxlength = "75" placeholder = "Task Title" value = "<?php echo htmlentities($taskTitle); ?>" required autofocus>      <input id="once" style="display: none" name="recurring" value="once" type="radio" checked>

        <label class = "formLabel">More Info</label>
        <textarea class = "inputTextBig" style = "margin: 0;" id = "moreInput" name = "moreInput" placeholder = "More Info"></textarea>
      </section>

      <section id = "editor__extra">
        <input id="repeat" style="display: none" name="recurring" value="repeat" type="radio">
        <label class = "formLabel">Start date</label>
        <!-- This is also the new and improved JS-powered .radioWrap! -->
        <section class = "radioWrap">
          <span class = "radioLabelWrap">
            <input id="startDateType--none" name="startDateType" value="none" type="radio" data-hide-sub = "customStartDateWrapper" <?php if ($defaultStartDate == "none") { echo "checked"; } ?>>
            <label for="startDateType--none" class="radioLabel">None</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="startDateType--today" name="startDateType" value="today" type="radio" data-hide-sub = "customStartDateWrapper" <?php if ($defaultStartDate == "today") { echo "checked"; } ?>>
            <label for="startDateType--today" class="radioLabel">Today</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="startDateType--custom" name="startDateType" value="custom" type="radio" data-show-sub = "customStartDateWrapper">
            <label for="startDateType--custom" class="radioLabel">Custom</label>
          </span>
        </section>
        <section class = "sub hidden" id = "customStartDateWrapper">
          <label class = "formLabel" for = "startDateInput">Custom Start Date</label>
          <input value = "<?php echo date("Y-m-d", strtotime("+1 day")) ?>" max = "<?php echo date("Y-m-d", strtotime("+1 day")) ?>" type = "date" class = "inputText" id = "startDateInput"  name = "startDateInput" minlength = "5" maxlength = "10" placeholder = "Deadline (YYYY-MM-DD)" required>
        </section>

        <label class = "formLabel">Deadline</label>
        <input value = "<?php echo date("Y-m-d", strtotime("+1 day")) ?>" type = "date" class = "inputText" id = "dateInput" name = "dateInput" minlength = "5" maxlength = "10" placeholder = "Deadline (YYYY-MM-DD)" required>


        <label class = "formLabel">Recurrence</label>
        <section class = "radioWrap">
          <span class = "radioLabelWrap">
            <input id="recurring--once" name="recurring" value="once" type="radio" data-hide-sub = "repeatD" checked>
            <label for="recurring--once" class="radioLabel">Off</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="recurring--repeat" name="recurring" value="repeat" type="radio" data-show-sub = "repeatD">
            <label for="recurring--repeat" class="radioLabel">On</label>
          </span>
        </section>

        <div id = "repeatD" class = "sub hidden">
          <label class = "formLabel">Frequency (days)</label>
          <input value = "7" type = "number" class = "inputText" name = "freqInput" min = "1" max = "1000" placeholder = "Repeat every x days" required>
          <label class = "formLabel">End date</label>
          <input value = "<?php echo date("Y-m-d", strtotime("+8 days")) ?>" type = "date" class = "inputText" name = "endInput" placeholder = "End date (MM/DD/YYYY)" required>
        </div>

        <label class = "formLabel">Priority</label>

        <section class = "radioWrap">
          <span class = "radioLabelWrap">
            <input id="priority--0" name="priority" value="0" type="radio">
            <label for="priority--0" class="radioLabel">Low</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="priority--1" name="priority" value="1" type="radio" checked>
            <label for="priority--1" class="radioLabel">Normal</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="priority--2" name="priority" value="2" type="radio">
            <label for="priority--2" class="radioLabel">High</label>
          </span>
        </section>

      </section>

      <input type = "hidden" name = "token" value = "<?php echo $_SESSION["userToken"]; ?>">

      <section id = "editor__actions">
        <input id = "subBtn" type = "submit" title = "Add | Ctrl + Enter" class = "inputBtn btn" name = "submitNew" value = "Add" style = "grid-column: 3" required>
      </section>

    </form>
  </section>

  <script defer>
    function getCurrentYYYYDate() {
      let date = new Date();
      let formattedDate = date.getFullYear() + "-";
      if (date.getMonth() < 9) {
        formattedDate += "0";
      }
      formattedDate += (date.getMonth() + 1) + "-";
      if (date.getDate() < 10) {
        formattedDate += "0";
      }
      formattedDate += date.getDate();
      return formattedDate;
    }
    document.querySelector("#dateInput").addEventListener("change", function() {
      document.querySelector("#startDateInput").max = this.value;
      if (document.querySelector("#startDateInput").value > document.querySelector("#dateInput").value) {
        document.querySelector("#startDateInput").value = document.querySelector("#dateInput").value;
      }
      // if start date is today & user sets deadline to before today
      if (document.querySelector("input[name='startDateType']:checked").value == "today" && document.querySelector("#startDateInput").value < getCurrentYYYYDate()) {
        // switch start date to custom (default to deadline)
        document.querySelector("input[name='startDateType'][value='custom']").click();
        document.querySelector("#startDateInput").value = document.querySelector("#dateInput").value;
      }
    });

    document.body.addEventListener('keydown', function(e) {
      if (e.key == "Enter" && e.ctrlKey) {
        document.querySelector(".inputTextBig").form.requestSubmit(document.querySelector("#subBtn"));
      }
    });

    // logic to automagically loop through sub visualizations!
    document.querySelectorAll("[data-show-sub]").forEach((el) => {
      el.addEventListener('click', () => {
        document.querySelector("#" + el.dataset.showSub).classList.remove("hidden");
      });
    })

    document.querySelectorAll("[data-hide-sub]").forEach((el) => {
      // get id stored, remove hidden attribute
      el.addEventListener('click', () => {
        document.querySelector("#" + el.dataset.hideSub).classList.add("hidden");
      });
    })
  </script>

<?php

  include("footer.php");

  }

  else {
    header("location: index.php");
  }

?>
