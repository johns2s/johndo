<?php
include("config.php");
require __DIR__ . '/vendor/autoload.php';
$purifierConfig = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($purifierConfig);

function new_template($conn) {
  $confirm_template = False;
  $template = "";
  while ($confirm_template == False) {
    $template = random_int(10000000, 100000000);
    $sqlCheckTemp = "SELECT * FROM tasks WHERE template = '$template'";
    $resultCheckTemp = mysqli_query($conn, $sqlCheckTemp);
    if (mysqli_num_rows($resultCheckTemp) == 0) {
      $confirm_template = True;
    }
  }
  return $template;
}

function postponeTask($conn, $id) {
  // move endDate forward to strtotime("tomorrow"). VERIFY NOT RECURRING.
  // uses ID not template due to issues mentioned in Gitlab issue tracking move away from these unique auto-incremented ids.
  $verifyNotRecurringSql = "SELECT freq, date FROM tasks WHERE id = '$id' LIMIT 1";
  $result = mysqli_query($conn, $verifyNotRecurringSql);

  if (mysqli_num_rows($result) == 1) {
    while ($row = mysqli_fetch_assoc($result)) {
      if ($row["freq"] == 0) {
        // only allow postponing if task NOT recurring.
        $newDate = strtotime("+1 day", $row["date"]);
        if ($row["date"] < strtotime("today")) {
          // if overdue
          $newDate = strtotime("today");
        }
        $sql = "UPDATE tasks set date = $newDate WHERE id = $id";
        mysqli_query($conn, $sql);
      }
    }
  }
  // Always fails silently & ungracefully. TODO.
}

/**
 * Returns user to currently-active homepage, not their default
 * 
 * @param array optional $URLparams 
 */
function return_to_current_homepage($customParams = array()) {
  // 
  $referer = $_SERVER["HTTP_REFERER"];

  // easiest way to ensure any superflous parameters are cleaned is to hardcoding possible return values (only have 2 of those)

  // if (preg_match(preg_match("(view=)((everything)|(today))", $referer))) {
  // ^^ regex to use if I ever need to ditch handcoded approach (more parameters)
  // after php 8 available (read: once i update my server), switch to str_contains()

  $params = array();
  if (strpos($referer, "view=today") > 0) {
    array_push($params, "view=today");
  }
  else if (strpos($referer, "view=everything") > 0) {
    array_push($params, "view=everything");
  }
  $params = array_merge($params, $customParams);

  $location = "index.php";
  if (!empty($params)) {
    $location .= "?" . implode("?", $params);
  }
  echo $location;

  header("location: " . $location);
  
}

if (isset($_SESSION["user"]) && ((isset($_POST["token"]) && $_POST["token"] == $_SESSION["userToken"])) || isset($_GET["token"]) && $_GET["token"] == $_SESSION["userToken"]) {
  if (isset($_POST["submitNew"])) {
    $title = mysqli_real_escape_string($conn, $_POST["titleInput"]);
    $more = mysqli_real_escape_string($conn, $purifier->purify($_POST["moreInput"]));
    $recurring = mysqli_real_escape_string($conn, $_POST["recurring"]);
    $startDateType = mysqli_real_escape_string($conn, $_POST["startDateType"]);
    $deadline = strtotime(mysqli_real_escape_string($conn, $_POST["dateInput"]));
    $priority = mysqli_real_escape_string($conn, $_POST["priority"]);

    // Figure out the start date
    if ($startDateType == "custom") {
      $startDate = strtotime(mysqli_real_escape_string($conn, $_POST["startDateInput"]));
    }
    else if ($startDateType == "today") {
      $startDate = strtotime("today");
      // WTF happens if the user is in a different timezone HUH???? Why are JS and PHP both handling dates??
    }
    else if ($startDateType == "none") {
      $startDate = $deadline;
    }

    //validation + additional variables
    if ($startDate > $deadline) {
        header("location: newtask.php?message=The+start+date+may+not+be+later+than+the+deadline.");
        exit;
    }

    if ($recurring == "repeat") {
      $freq = mysqli_real_escape_string($conn, $_POST["freqInput"]);
      $endDate = strtotime(mysqli_real_escape_string($conn, $_POST["endInput"]));
      if ($deadline > $endDate) {
        header("location: newtask.php?message=Recurring+task+must+end+after+first+deadline.");
        exit;
      }
      if ($freq < 1 || $freq > 1000) {
        header("location: newtask.php?message=Invalid+frequency.");
        exit;
      }
    }

    else {
      $freq = 0;
      $endDate = False;
    }

    $user = $_SESSION["userID"];
    $template = new_template($conn);

		$sql = "INSERT INTO tasks set template = '$template', title = '$title', more = '$more', startDate = '$startDate', date = '$deadline', endDate = '$endDate', freq = '$freq', priority = $priority, user = '$user'";
		if (mysqli_query($conn, $sql)) {
      header("location: index.php");
      exit;
		}
    else {
  		header("location: newtask.php?message=A+database+error+occured+while+adding+your+task.");
      exit;
  	}
  }

  else if (isset($_POST["submitUpdate"])) {
    $template = mysqli_real_escape_string($conn, $_POST["taskID"]);
    $title = mysqli_real_escape_string($conn, $_POST["titleInput"]);
    $more = mysqli_real_escape_string($conn, $purifier->purify($_POST["moreInput"]));
    $recurring = mysqli_real_escape_string($conn, $_POST["recurring"]);
    $deadline = strtotime(mysqli_real_escape_string($conn, $_POST["dateInput"]));
    $startDateType = mysqli_real_escape_string($conn, $_POST["startDateType"]);
    $priority = mysqli_real_escape_string($conn, $_POST["priority"]);

    // Figure out the start date
    if ($startDateType == "custom") {
      $startDate = strtotime(mysqli_real_escape_string($conn, $_POST["startDateInput"]));
    }
    else if ($startDateType == "today") {
      $startDate = strtotime("today");
      // WTF happens if the user is in a different timezone HUH???? Why are JS and PHP both handling dates??
    }
    else if ($startDateType == "none") {
      $startDate = $deadline;
    }

    //validation + additional variables
    if ($startDate > $deadline) {
        header("location: edittask.php?message=The+start+date+may+not+be+later+than+the+deadline.");
        exit;
    }

    if ($recurring == "repeat") {
      $freq = mysqli_real_escape_string($conn, $_POST["freqInput"]);
      $endDate = strtotime(mysqli_real_escape_string($conn, $_POST["endInput"]));
      if ($deadline > $endDate) {
        header("location: edittask.php?message=Recurring+task+must+end+after+first+deadline.");
        exit;
      }
      if ($freq < 1 || $freq > 1000) {
        header("location: edittask.php?message=Invalid+frequency.");
        exit;
      }
    }

    else {
      $freq = 0;
      $endDate = False;
    }

    $user = $_SESSION["userID"];

		$sql = "UPDATE tasks set title = '$title', more = '$more', startDate = '$startDate', date = '$deadline', endDate = '$endDate', freq = '$freq', priority = $priority, user = '$user' WHERE template = '$template'";
		if (mysqli_query($conn, $sql)) {
      header("location: index.php");
      exit;
		}
    else {
  		header("location: edittask.php?message=A+database+error+occured+while+editing+your+task.");
      exit;
  	}
  }

  else if (isset($_GET["postpone"]) && isset($_GET["id"])) {
    postponeTask($conn, $_GET["id"]);
    return_to_current_homepage();
    exit;  

  }

  else if (isset($_POST["postponeAllOverdue"]) && isset($_POST["ids"])) {
    // move endDate forward to strtotime("tomorrow"). VERIFY NOT RECURRING.
    // uses ID not template due to issues mentioned in Gitlab issue tracking move away from these unique auto-incremented ids.
    $ids = explode(',', $_POST["ids"]);

    foreach ($ids as $id) {
      postponeTask($conn, $id);
    }
    exit;  

  }

  else if (isset($_GET["split"]) && isset($_GET["id"])) {
    // turn instance of a recurring task into a new task AND trigger recurrence.
    $id = mysqli_real_escape_string($conn, $_GET["id"]);
    $user = $_SESSION["userID"];

    // get what is needed from db
    $sqlCheck = "SELECT * FROM tasks WHERE user = '$user' AND id = '$id'";
    $result = mysqli_query($conn, $sqlCheck);

    if (mysqli_num_rows($result) == 1) {
      $today = strtotime("today");
      while ($row = mysqli_fetch_assoc($result)) {
        $freq = $row["freq"];
        $deadline = strtotime("+" . $freq . " days", $row["date"]);
        
        if ($row["freq"] > 0 && $row["endDate"] > $deadline) {
          // go for reccur
          $template = $row["template"];
          $title = $row["title"];
          $more = $row["more"];
          $startDate = strtotime("+" . $freq . " days", $row["startDate"]);
          $endDate = $row["endDate"];

          $sql = "INSERT INTO tasks set template = '$template', title = '$title', more = '$more', startDate = '$startDate', date = '$deadline', endDate = '$endDate', freq = '$freq', user = '$user'";
          if (!mysqli_query($conn, $sql)) {
            header("location: newtask.php?message=A+database+error+occured+while+recurring+your+task.");
            exit;
          }
        }
      }
    }

    // Give existing task a new template & get rid of recurrence
    $template = new_template($conn);
    $freq = 0;
    $endDate = False;
    $sql = "UPDATE tasks SET template = '$template', freq = '$freq', endDate = '$endDate' WHERE id = '$id' AND user = '$user' ";
  	if (mysqli_query($conn, $sql)) {
  		return_to_current_homepage();
      exit;
    }
  	else {
      return_to_current_homepage(["message=Error+splitting+task."]);
      exit;
    }
  }

  else if (isset($_GET["delete"]) && isset($_GET["deleteInstance"])) {
    $id = mysqli_real_escape_string($conn, $_GET["id"]);
    $user = $_SESSION["userID"];

    // get what is needed from db
    $sqlCheck = "SELECT * FROM tasks WHERE user = '$user' AND id = '$id'";
    $result = mysqli_query($conn, $sqlCheck);

    if (mysqli_num_rows($result) == 1) {
      $today = strtotime("today");
      while ($row = mysqli_fetch_assoc($result)) {
        $freq = $row["freq"];
        $deadline = strtotime("+" . $freq . " days", $row["date"]);
        
        if ($row["freq"] > 0 && $row["endDate"] > $deadline) {
          // go for reccur
          $template = $row["template"];
          $title = $row["title"];
          $more = $row["more"];
          $startDate = strtotime("+" . $freq . " days", $row["startDate"]);
          $endDate = $row["endDate"];

          $sql = "INSERT INTO tasks set template = '$template', title = '$title', more = '$more', startDate = '$startDate', date = '$deadline', endDate = '$endDate', freq = '$freq', user = '$user'";
          if (!mysqli_query($conn, $sql)) {
            header("location: newtask.php?message=A+database+error+occured+while+recurring+your+task.");
            exit;
          }
        }
      }
    }


    $sql = "DELETE FROM tasks WHERE id = '$id' AND user = '$user'";
  	if (mysqli_query($conn, $sql)) {
  		return_to_current_homepage();
      exit;
    }
  	else {
      return_to_current_homepage(["message=Error+deleting+task."]);
      exit;
    }
  }

  else if (isset($_GET["delete"]) && isset($_GET["deleteAll"])) {
    $template = mysqli_real_escape_string($conn, $_GET["template"]);
    $userID = $_SESSION["userID"];
    $sql = "DELETE FROM tasks WHERE template = '$template' AND user = '$userID'";
    if (mysqli_query($conn, $sql)) {
      header("location: index.php");
      exit;
    }
    else {
      header("location: index.php?message=Error+deleting+task.");
      exit;
    }
  }

  else if (isset($_POST["submitUpdateSettingsPW"])) {
    $passwordText = mysqli_real_escape_string($conn, $_POST["passwordChangeInput"]);
    $userID = $_SESSION["userID"];
    if (strlen($passwordText) < 8) {
      header("location: settings.php?message=Password+too+short.");
      exit;
    }
    $passwordNew = password_hash($passwordText, PASSWORD_DEFAULT);
    $sql = "UPDATE users SET password = '$passwordNew' WHERE id = '$userID';";

    if (mysqli_query($conn, $sql)) {
      header("location: settings.php");
      exit;
    }
    else {
      header("location: settings.php?message=Something+went+wrong.+You+may+not+have+the+permissions+to+do+this.");
      exit;
    }
  }

  else if (isset($_POST["submitUpdateSettings"])) {
    $fuzziness = mysqli_real_escape_string($conn, $_POST["fuzziness"]);
    $view = mysqli_real_escape_string($conn, $_POST["view"]);
    $workweekMode = (int)mysqli_real_escape_string($conn, $_POST["workweekMode"]);
    $email = mysqli_real_escape_string($conn, $_POST["emailChangeInput"]);
    $defaultStartDate = mysqli_real_escape_string($conn, $_POST["defaultStartDate"]);
    $userID = $_SESSION["userID"];
    $updateSettingsSQL = "UPDATE settings set fuzzyDates = '$fuzziness', view = '$view', workweekMode = $workweekMode, defaultStartDate = '$defaultStartDate' WHERE user = '$userID'";
    $updateEmailSQL = "UPDATE users SET email = '$email' WHERE id = '$userID';";

    if (mysqli_query($conn, $updateSettingsSQL) && mysqli_query($conn, $updateEmailSQL)) {
      header("location: settings.php");
      exit;
    }

    else {
      header("location: settings.php?message=Something+went+wrong.+You+may+not+have+the+permissions+to+do+this.");
      exit;
    }

  }


  else {
    header("location: index.php");
    exit;
  }

}

else {
  header("location: index.php");
  exit;
}

mysqli_close($conn);
