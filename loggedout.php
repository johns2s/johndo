<?php
?>
<section class = "bigHeader">
  <section class = "bigHeader__content">
    <h1>To do? More like <b>too done</b>.</h1>
    <section class = "bigHeader__content__more">
      <span class = "bigHeader__content__subtitle">Get your tasks done with johnDo</span>
    </section>
  </section>
</section>

<section class = "normalWidthWrapper">
  <section class = "homepageContent">
    <section class = "homepageContent__hero" style = "grid-column: 1 / -1">
      <h2>Outsource the minutiae.</h2>
      <span class = "details">We don't try to be everything for everyone, enabling a streamlined, intuitive, and functional experience. Just input your tasks and our magic will make sure you're always on track.</span>
      <a href = 'signup.php' class="chip chip--bare">
        <span class="chip__text">Signup for free</span>
        <span class="material-icons" style="font-size: 1.3em;">arrow_forward</span>
      </a>
    </section>
    <section class = "homepageContent__feature">
      <span class = "material-icons homepageContent__feature__icon">devices</span>
      <section class = "homepageContent__feature__text">
        <h3>Works everywhere</h3>
        <span>johnDo's beautiful and responsive design is hand-crafted to work great on all your phones, tablets, and computers.</span>
      </section>
    </section>

    <section class = "homepageContent__feature">
      <span class = "material-icons homepageContent__feature__icon">bolt</span>
      <section class = "homepageContent__feature__text">
        <h3>Really fast</h3>
        <span>johnDo's limited third-party resources and simple codebase provides a bloat-free, lightning-fast experience.</span>
      </section>
    </section>
    <section class = "homepageContent__feature">
      <span class = "material-icons homepageContent__feature__icon">shield</span>
      <section class = "homepageContent__feature__text">
        <h3>Private by design</h3>
        <span>We have no analytics. We don't serve ads. We never sell your data. And all your data is purged immediately if you choose to delete your account.</span>
      </section>
    </section>
    <section class = "homepageContent__feature">
      <span class = "material-icons homepageContent__feature__icon">code</span>
      <section class = "homepageContent__feature__text">
        <h3>Open source</h3>
        <span>johnDo is 100% open source and licensed under the Mozilla Public License, meaning anyone can view or contribute to johnDo's code.</span>
      </section>
    </section>


  </section>
</section>
<?php
?>
