<?php

  include("header.php");

  if (isset($_SESSION["user"]) && isset($_GET["template"])) {

    $taskID = $_GET["template"];
    $userID = $_SESSION["userID"];

    $sql = "SELECT * FROM tasks WHERE template = '$taskID' AND user = '$userID'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)) {
        $taskDate = date("Y-m-d", $row["date"]);
        $startDate = date("Y-m-d", $row["startDate"]);
        
        if ($row["freq"] == 0) {
          $recurring = FALSE;
          $freq = 7;
          $endDateOrig = $taskDate;
          $endDate = date('Y-m-d', strtotime($endDateOrig . ' + 7 days'));
        }
        else {
          $recurring = TRUE;
          $endDateOrig = date("Y-m-d", $row["date"]);
          $endDate = date('Y-m-d', $row["endDate"]);
          $freq = $row["freq"];
        }

        $freqCheck = $row["freq"];
        $taskTitle = $row["title"];
        $taskMore = $row["more"];
        $priority = $row["priority"];

        if ($taskDate == $startDate) {
          $startDateType = "none";
        }
        else {
          $startDateType = "custom";
        }
      }
    }

    else {
      header("location: index.php");
    }

    $confirmDelete;

    if ($freqCheck == 0) {
      // Not Recurring
      $confirmDelete = "Are you sure you want to delete this task?";
    }


    else {
      // Recurring
      $confirmDelete = "This is a recurring task. If you delete it from this page, it will not continue to recur.";
    }

?>

<script>
  function alertDelete() {
    var answer = confirm("<?php echo $confirmDelete; ?>");
    if (answer) {
      window.location = "process.php?delete&deleteAll&template=<?php echo $taskID ?>&token=<?php echo $_SESSION['userToken'] ?>";
    }
  }
</script>

<script src="https://cdn.tiny.cloud/1/2og7s3xdcmvdj7cc6q76gpmwvvybp8b9scchl4ag1l3oerel/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#moreInput',
    toolbar_mode: 'floating',
    toolbar: 'undo redo | h1 | bold italic link | bullist numlist | outdent indent | insertDoneIcon',
    plugins: 'autolink autoresize lists advlist link charmap',
    menubar: false,
    contextmenu: false,
    skin: (window.matchMedia("(prefers-color-scheme: dark)").matches ? "oxide-dark" : ""),
    content_css: (window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : ""),
    init_instance_callback: function (editor) {
      editor.shortcuts.add(
        'meta+13', "Submit form", function () {
          document.querySelector(".inputTextBig").form.requestSubmit(document.querySelector("#subBtn"));
      });
      editor.shortcuts.add(
        'meta+d', "Insert done icon", function () {
          editor.insertContent('✅ ');
      });
    },
    setup: function (editor) {
      editor.ui.registry.addButton('insertDoneIcon', {
        text: '✅',
        tooltip: 'Insert done icon',
        onAction: function (_) {
          editor.insertContent('✅');
        }
      });
    }
  });</script>

<section class = "bigHeader">
  <section class = "bigHeader__content">
    <h1>Edit task</h1>
    <section class = "bigHeader__content__more">
      <span class = "bigHeader__content__subtitle">It's <?php echo date("l, F jS"); ?>.</span>
    </section>
    <?php
      if (isset($_GET["message"])) {
        echo "
        <section class = 'bigHeader__content__more'>
          <section class = 'bigHeader__content__message'>
            <span class='material-icons'>error_outline</span>
            <span class = 'bigHeader__content__subtitle'>" . htmlentities($_GET['message']) . "</span>
          </section>
        </section>";
      }
    ?>

  </section>
</section>

  <section class = "editorWrap">
    <form class = "item editor" action = "process.php" method = "POST">
      <section id = "editor__main">
        <label class = "formLabel">Task Name</label>
        <input type = "text" class = "inputText" name = "titleInput" maxlength = "75" placeholder = "Task Title" value = "<?php echo htmlentities($taskTitle); ?>" required>
        
        <label class = "formLabel">More Info</label>
        <textarea id = "moreInput" class = "inputTextBig" style = "margin: 0;" name = "moreInput" placeholder = "More Info"><?php echo htmlentities($taskMore); ?></textarea>
      </section>
      <section id = "editor__extra">
        <label class = "formLabel">Start date</label>
        <!-- This is also the new and improved JS-powered .radioWrap! -->
        <section class = "radioWrap">
          <span class = "radioLabelWrap">
            <input id="startDateType--none" name="startDateType" value="none" type="radio" data-hide-sub = "customStartDateWrapper" <?php if ($startDateType == "none") echo "checked" ?>>
            <label for="startDateType--none" class="radioLabel">None</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="startDateType--today" name="startDateType" value="today" type="radio" data-hide-sub = "customStartDateWrapper">
            <label for="startDateType--today" class="radioLabel">Today</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="startDateType--custom" name="startDateType" value="custom" type="radio" data-show-sub = "customStartDateWrapper" <?php if ($startDateType == "custom") echo "checked" ?>>
            <label for="startDateType--custom" class="radioLabel">Custom</label>
          </span>
        </section>
        <section class = "sub hidden" id = "customStartDateWrapper">
          <label class = "formLabel" for = "startDateInput">Custom Start Date</label>
          <input value = "<?php echo htmlentities($startDate) ?>" max = "<?php htmlentities($taskDate) ?>" type = "date" class = "inputText" id = "startDateInput"  name = "startDateInput" minlength = "5" maxlength = "10" placeholder = "Deadline (YYYY-MM-DD)" required>
        </section>

        <label class = "formLabel">Deadline</label>
        <input value = "<?php echo htmlentities($taskDate); ?>" type = "date" id = "dateInput" class = "inputText" name = "dateInput" minlength = "5" maxlength = "10" placeholder = "Deadline (YYYY-MM-DD)" required>

        <label class = "formLabel">Recurrence</label>
        <section class = "radioWrap">
          <span class = "radioLabelWrap">
            <input id="recurring--once" name="recurring" value="once" type="radio" data-hide-sub = "repeatD" <?php if ($freqCheck == False) { echo "checked"; } ?>>
            <label for="recurring--once" class="radioLabel">Off</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="recurring--repeat" name="recurring" value="repeat" type="radio" data-show-sub = "repeatD" <?php if ($freqCheck != False) { echo "checked"; } ?>>
            <label for="recurring--repeat" class="radioLabel">On</label>
          </span>
        </section>
        <div id = "repeatD" class = "sub <?php if(!$freqCheck) echo 'hidden' ?>">
          <label class = "formLabel">Frequency (days)</label>
          <input value = "<?php echo htmlentities($freq); ?>" type = "number" class = "inputText" name = "freqInput" min = "1" max = "1000" placeholder = "Repeat every x days" required>
          <label class = "formLabel">End date</label>
          <input value = "<?php echo htmlentities($endDate); ?>" type = "date" class = "inputText" name = "endInput" placeholder = "End date (MM/DD/YYYY)" required>
        </div>

        <label class = "formLabel">Priority</label>

        <section class = "radioWrap">
          <span class = "radioLabelWrap">
            <input id="priority--0" name="priority" value="0" type="radio" <?php if ($priority == 0) echo "checked" ?>>
            <label for="priority--0" class="radioLabel">Low</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="priority--1" name="priority" value="1" type="radio" <?php if ($priority == 1) echo "checked" ?>>
            <label for="priority--1" class="radioLabel">Normal</label>
          </span>
          <span class = "radioLabelWrap">
            <input id="priority--2" name="priority" value="2" type="radio" <?php if ($priority == 2) echo "checked" ?>>
            <label for="priority--2" class="radioLabel">High</label>
          </span>
        </section>
      </section>
      
      <input type = "hidden" name = "token" value = "<?php echo $_SESSION["userToken"]; ?>">
      <input type = "hidden" name = "taskID" value = "<?php echo htmlentities($taskID); ?>">
      <section id = "editor__actions">
        <button type = "button" onclick = "alertDelete()" title = "Delete task" class = "material-icons btn" style = "color: #f62626; padding: 0; grid-column: 2;">delete</button>
        <input id = "subBtn" type = "submit" title = "Save | Ctrl + Enter" class = "inputBtn btn" name = "submitUpdate" value = "Save" style = "grid-column: 3" required>
      </section>
    </form>
  </section>

  <script defer>
  function getCurrentYYYYDate() {
    let date = new Date();
    let formattedDate = date.getFullYear() + "-";
    if (date.getMonth() < 9) {
      formattedDate += "0";
    }
    formattedDate += (date.getMonth() + 1) + "-";
    if (date.getDate() < 10) {
      formattedDate += "0";
    }
    formattedDate += date.getDate();
    return formattedDate;
  }
  document.querySelector("#dateInput").addEventListener("change", function() {
    document.querySelector("#startDateInput").max = this.value;
    if (document.querySelector("#startDateInput").value > document.querySelector("#dateInput").value) {
      document.querySelector("#startDateInput").value = document.querySelector("#dateInput").value;
    }
    // if start date is today & user sets deadline to before today
    if (document.querySelector("input[name='startDateType']:checked").value == "today" && document.querySelector("#startDateInput").value < getCurrentYYYYDate()) {
      // switch start date to custom (default to deadline)
      document.querySelector("input[name='startDateType'][value='custom']").click();
      document.querySelector("#startDateInput").value = document.querySelector("#dateInput").value;
    }
  });


  document.body.addEventListener('keydown', function(e) {
    if (e.key == "Enter" && e.ctrlKey) {
      document.querySelector(".inputTextBig").form.requestSubmit(document.querySelector("#subBtn"));
    }
  });

  window.addEventListener("load", () => {
    if (document.querySelector('input[name="startDateType"]:checked').value == "custom") {
      document.querySelector("#customStartDateWrapper").classList.remove("hidden");
    }
  }); 

  // logic to automagically loop through sub visualizations!
  document.querySelectorAll("[data-show-sub]").forEach((el) => {
    el.addEventListener('click', () => {
      document.querySelector("#" + el.dataset.showSub).classList.remove("hidden");
    });
  })

  document.querySelectorAll("[data-hide-sub]").forEach((el) => {
    // get id stored, remove hidden attribute
    el.addEventListener('click', () => {
      document.querySelector("#" + el.dataset.hideSub).classList.add("hidden");
    });
  })



  </script>

<?php

  include("footer.php");

  }

  else {
    header("location: index.php");
  }

?>
