<?php

  include("header.php");

  if (isset($_SESSION["user"])) {

    $userID = $_SESSION["userID"];

    $sqlSet = "SELECT * FROM settings WHERE user = '$userID' LIMIT 1";
    $resultSet = mysqli_query($conn, $sqlSet);
    if (mysqli_num_rows($resultSet) == 1) {
      while($rowSet = mysqli_fetch_assoc($resultSet)) {
        $fuzziness = $rowSet["fuzzyDates"];
        $view = $rowSet["view"];
        $workweekMode = $rowSet["workweekMode"];
        $overdueExpand = $rowSet["overdueExpand"];
        $defaultStartDate = $rowSet["defaultStartDate"];
      }
    }
    // get user's email
    $sqlEmail = "SELECT email FROM users WHERE id = '$userID' LIMIT 1";
    $resultEmail = mysqli_query($conn, $sqlEmail);
    $userEmail = "";
    if (mysqli_num_rows($resultEmail) == 1) {
      while($rowEmail = mysqli_fetch_assoc($resultEmail)) {
        $userEmail = $rowEmail["email"];
      }
    }
    else {
      header("location: index.php");
    }


?>

  <section class = "bigHeader">
    <section class = "bigHeader__content">
      <h1>Settings</h1>
      <?php
        if (isset($_GET["message"])) {
          echo "
          <section class = 'bigHeader__content__more'>
            <section class = 'bigHeader__content__message'>
              <span class='material-icons'>error_outline</span>
              <span class = 'bigHeader__content__subtitle'>" . htmlentities($_GET['message']) . "</span>
            </section>
          </section>";
        }
      ?>
    </section>
  </section>

  <section class = "normalWidthWrapper">
    <script>
      function alertKill() {
        var answer = confirm("Are you sure you wish to permanently delete your account, all of your tasks, and any other associated data?");
        if (answer) {
          window.location = "logsign.php?killAccount=True&token=<?php echo $_SESSION['userToken'] ?>";
        }
      }

      function changeView(el) {
        if (el.dataset.viewActive == 1) return; // New view == old view

        // determine if new view is above or below the current view; then choose appropriate transition
        let transition =
          (document.querySelector(".settingsNavButton[data-view-active='1']").dataset.viewIndex < el.dataset.viewIndex)
          ? "fadeInFromAfter" : "fadeInFromBefore";
        
          document.querySelectorAll(".settingsView").forEach((item) => {
          item.classList.remove("settingsView--visible");
          item.classList.remove("settingsView--fadeInFromAfter");
          item.classList.remove("settingsView--fadeInFromBefore");
        });
        document.querySelectorAll(".settingsNavButton").forEach((item) => {
          item.classList.remove("settingsNavButton--active");
          item.dataset.viewActive = 0;
        });

        el.classList.add("settingsNavButton--active");
        el.dataset.viewActive = 1;
        document.getElementById(el.dataset.view + "SettingsView").classList.add("settingsView--visible", "settingsView--" + transition);
        document.querySelector(".settingsSidebar").classList.remove("settingsSidebar--open");
      }
  </script>

    <div class = "item settings" action = "process.php" method = "POST">
      <section class = "settingsSidebar">
        <!-- One line to remove whitespace on mobile nav layout -->
        <button onclick = "changeView(this)" data-view = "general" data-view-active = "1" data-view-index = "0" class = "settingsNavButton settingsNavButton--active">General</button><button onclick = "changeView(this)" data-view = "password" data-view-active = "0" data-view-index = "1" class = "settingsNavButton">Password</button><button onclick = "changeView(this)" data-view = "data" data-view-active = "0" data-view-index = "2" class = "settingsNavButton">Your Data</button><button onclick = "changeView(this)" data-view = "shortcuts" data-view-active = "0" data-view-index = "3" class = "settingsNavButton">Shortcuts</button>
      </section>

      <section class = "settingsMain">
        <form action = "process.php" method = "POST" id = "generalSettingsView" class = "settingsView settingsView--visible">
          <input type = "hidden" name = "token" value = "<?php echo $_SESSION["userToken"]; ?>">
          <label class = "formLabel">Date display (only applies to Everything view)</label>

          <section class = "radioWrap">
            <span class = "radioLabelWrap">
              <input id="fuzziness--fuzzy" name="fuzziness" value="fuzzy" type="radio" <?php if($fuzziness == "fuzzy") { echo "checked"; } ?>>
              <label for="fuzziness--fuzzy" class="radioLabel">Fuzzy</label>
            </span>
            <span class = "radioLabelWrap">
              <input id="fuzziness--semi" name="fuzziness" value="semiFuzzy" type="radio" <?php if($fuzziness == "semiFuzzy") { echo "checked"; } ?>>
              <label for="fuzziness--semi" class="radioLabel">Hybrid</label>
            </span>
            <span class = "radioLabelWrap">
            <input id="fuzziness--not" name="fuzziness" value="notFuzzy" type="radio" <?php if($fuzziness == "notFuzzy") { echo "checked"; } ?>>
              <label for="fuzziness--not" class="radioLabel">Exact</label>
            </span>
          </section>

          <label class = "formLabel">Default task start date</label>
          <section class = "radioWrap">
            <span class = "radioLabelWrap">
              <input id="defaultStartDate--none" name="defaultStartDate" value="none" type="radio" <?php if($defaultStartDate == "none") { echo "checked"; } ?>>
              <label for="defaultStartDate--none" class="radioLabel">None</label>
            </span>
            <span class = "radioLabelWrap">
              <input id="defaultStartDate--today" name="defaultStartDate" value="today" type="radio" <?php if($defaultStartDate == "today") { echo "checked"; } ?>>
              <label for="defaultStartDate--today" class="radioLabel">Day of Creation</label>
              <script>console.log(<?php echo $overdueExpand ?>);</script>
            </span>
          </section>


          <label class = "formLabel">Default view</label>
          <section class = "radioWrap">
            <span class = "radioLabelWrap">
              <input id="view--today" name="view" value="today" type="radio" <?php if($view == "today") { echo "checked"; } ?>>
              <label for="view--today" class="radioLabel">Today</label>
            </span>
            <span class = "radioLabelWrap">
              <input id="view--everything" name="view" value="everything" type="radio" <?php if($view == "everything") { echo "checked"; } ?>>
              <label for="view--everything" class="radioLabel">Everything</label>
            </span>
          </section>

          <label class = "formLabel" title = "During the weekend, tasks due the following Monday are distinguished">Workweek mode</label>
          <section class = "radioWrap">
            <span class = "radioLabelWrap">
              <input id="workweekMode--off" name="workweekMode" value="0" type="radio" <?php if($workweekMode == 0) { echo "checked"; } ?>>
              <label for="workweekMode--off" class="radioLabel">Off</label>
            </span>
            <span class = "radioLabelWrap">
              <input id="workweekMode--on" name="workweekMode" value="1" type="radio" <?php if($workweekMode == 1) { echo "checked"; } ?>>
              <label for="workweekMode--on" class="radioLabel">On</label>
            </span>
          </section>

          <label class = "formLabel">Email Address</label>
          <input type = "email" class = "inputText" name = "emailChangeInput" placeholder = "New Email Address" value = "<?php echo $userEmail; ?>" required>
          <input type = "submit" class = "inputBtn" name = "submitUpdateSettings" value = "Save" required>
        </form>

        <form id = "passwordSettingsView" class = "settingsView" action = "process.php" method = "POST">
          <input type = "hidden" name = "token" value = "<?php echo $_SESSION["userToken"]; ?>">
          <label class = "formLabel">New Password</label>
          <input type = "password" class = "inputText" name = "passwordChangeInput" value = "" placeholder = "New Password" autocomplete = "new-password" minlength = "8" required>

          <input type = "submit" class = "inputBtn" name = "submitUpdateSettingsPW" value = "Save" required>
        </form>

        <div id = "dataSettingsView" class = "settingsView">
          <p class = "message">While we collect other data, like display preferences, it is not personal.</p>
          <label class = "formLabel">Your email</label>
          <div class = "sub">
            <p class = "data"><?php echo $userEmail; ?></p>
          </div>
          <label class = "formLabel">Your password</label>
          <div class = "sub">
            <p class = "data"><a href = "https://www.php.net/manual/en/function.password-hash.php">Hashed</a> for your security. Even we don't know it.</p>
          </div>

          <a href = 'index.php?view=everything' class="chip chip--bare">
            <span class="chip__text">See all tasks</span>
            <span class="material-icons" style="font-size: 1.3em;">arrow_forward</span>
          </a>


          <button id = "deleteAccountInputBtn" type = "button" onclick = "alertKill()" class = "inputBtn">Delete Account & Data</a>
        </div>
        <div id = "shortcutsSettingsView" class = "settingsView">
          <section>
            <h2>General</h2>
            <ul class = "shortcutList">
              <li class = "shortcut">
                <span class = "shortcut__description">Save task</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key">CTRL</span><span class = "shortcut__key shortcut__key--wide">ENTER</span></section>
              </li>
              <li class = "shortcut">
                <span class = "shortcut__description">New task (from home)</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key shortcut__key--narrow">t</span></section>
              </li>
              <li class = "shortcut">
                <span class = "shortcut__description">Show overdue tasks (from Today view)</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key shortcut__key--narrow">o</span></section>
              </li>            
            </ul>
          </section>
          <section>
          <h2>Rich text editing</h2>
            <ul class = "shortcutList">
              <li class = "shortcut">
                <span class = "shortcut__description">Bold</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key">CTRL</span><span class = "shortcut__key shortcut__key--narrow">B</span></section>
              </li>
              <li class = "shortcut">
                <span class = "shortcut__description">Italic</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key">CTRL</span><span class = "shortcut__key shortcut__key--narrow">I</span></section>
              </li>



              <li class = "shortcut">
                <span class = "shortcut__description">Insert heading</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key">ALT</span><span class = "shortcut__key shortcut__key--wide">SHIFT</span><span class = "shortcut__key shortcut__key--narrow">1</span></section>
              </li>
              <li class = "shortcut">
                <span class = "shortcut__description">Insert link</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key">CTRL</span><span class = "shortcut__key shortcut__key--narrow">K</span></section>
              </li>
              <li class = "shortcut">
                <span class = "shortcut__description">Insert checkmark</span>
                <section class = "shortcut__keyWrap"><span class = "shortcut__key">CTRL</span><span class = "shortcut__key shortcut__key--narrow">D</span></section>
              </li>
              <li style = "font-style: italic">Markdown syntax supported: *, #, etc</li>
            </ul>
          </section>
        </div>
      </section>
    </div>
  </section>
<?php

  include("footer.php");

  }

  else {
    header("location: index.php");
  }

?>
