<?php
?>

<h2 class = "headline">Today is <?php echo date("l, F dS") ?></h2>

<?php

if (isset($_GET["message"])) {
  echo "<p class = 'message'><b>" . htmlentities($_GET['message']) . "</b></p>";
}

?>

<div class = "wrapper tskW">
  <div class = "item tsk">
    <h3 style = "margin-bottom: 25px">Add New Task</h3>
    <hr>
    <form action = "newtask.php" method = "POST">
      <label class = "formLabel">Task Title</label>
      <input type = "text" class = "inputText" name = "titleInput" maxlength = "75" placeholder = "Task Title">
      <input type = "submit" class = "inputBtn" name = "submitNewTitle" value = "Continue..." required>
    </form>
  </div>

  <?php

    $userID = $_SESSION["userID"];

    $sqlSet = "SELECT * FROM settings WHERE user = '$userID' LIMIT 1";
    $resultSet = mysqli_query($conn, $sqlSet);
    if (mysqli_num_rows($resultSet) == 1) {
      while($rowSet = mysqli_fetch_assoc($resultSet)) {
        $fuzziness = $rowSet["fuzzyDates"];
        $workweekMode = $rowSet["workweekMode"];
      }
    }
    else {
      $fuzziness = "notFuzzy";
    }

    $sql = "SELECT * FROM tasks WHERE user = '$userID' ORDER BY date asc, priority desc, id desc";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)) {
        if ($row["date"] == "unknown") {
          $date = $row["date"];
          $dite = $row["date"];
        }
        else {
          $date = date("D, M jS", $row["date"]);
          $dite = date("m/d/Y", $row["date"]);
        }

        echo "<div class = 'item tsk'><h3>" . htmlentities($row["title"]) . "</h3>";

        $more = $row["more"];

          if ($fuzziness == "fuzzy") {

              if (date("m/d/Y", strtotime("yesterday")) == $dite) {
                $formattedDate = "Yesterday";
              }
              else if (date("m/d/Y", strtotime("tomorrow")) == $dite) {
                $formattedDate = "Tomorrow";
              }
              else if (date("m/d/Y") == $dite) {
                $formattedDate = "Today";
              }
              else if (date("m/d/Y") < $dite && date("m/d/Y", strtotime("+7 days")) > $dite) {
                $formattedDate = "a few days";
              }
              else if (date("m/d/Y") > $dite && date("m/d/Y", strtotime("-7 days")) < $dite) {
                $formattedDate = "a few days ago";
              }
              else {
                $formattedDate = $date;
              }


              if ((strtotime("tomorrow") >= $row["date"] || ($workweekMode == 1 && in_array(date("N"), array(5, 6, 7)) && date("N", $row["date"]) == 1 && (date("z", $row["date"]) - date("z")) < 5)) && strtotime("today") <= $row["date"] && $row["date"] !== "unknown") {
                echo "<img class = 'icon' src = 'static/redclock.avif' alt = 'This task is due soon' title = 'This task is due soon'></img>
                <span class = 'taskCard__date' style = 'color: #f62626'><b>" . $formattedDate . "</b></span>
                <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";
              }
              else if (strtotime("today") >= $row["date"] && $row["date"] !== "unknown") {
                echo "<img class = 'icon' src = 'static/warning.avif' alt = 'This task is late' title = 'This task is late'></img>
                <span class = 'taskCard__date' style = 'color: #f62626'><b>" . $formattedDate . "</b></span>
                <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";

              }
              else {
                echo "<img class = 'icon' src = 'static/clock.avif'></img>
                <span class = 'taskCard__date'><b>" . $formattedDate . "</b></span>
                <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";
              }
            }

            else if ($fuzziness == "semiFuzzy") {
                if (date("m/d/Y", strtotime("yesterday")) == $dite) {
                  $formattedDate = "Yesterday";
                }
                else if (date("m/d/Y", strtotime("tomorrow")) == $dite) {
                  $formattedDate = "Tomorrow";
                }
                else if (date("m/d/Y") == $dite) {
                  $formattedDate = "Today";
                }
                else if (date("m/d/Y") < $dite && date("m/d/Y", strtotime("+7 days")) > $dite) {
                  $formattedDate = date("l", $row["date"]);
                }
                else {
                  $formattedDate = $date;
                }

                if ((strtotime("tomorrow") >= $row["date"] || ($workweekMode == 1 && in_array(date("N"), array(5, 6, 7)) && date("N", $row["date"]) == 1 && (date("z", $row["date"]) - date("z")) < 5)) && strtotime("today") <= $row["date"] && $row["date"] !== "unknown") {
                  echo "<img class = 'icon'alt = 'This task is due soon'  src = 'static/redclock.avif' title = 'This task is due soon'></img>
                  <span class = 'taskCard__date' style = 'color: #f62626'><b>" . $formattedDate . "</b></span>
                  <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";
                }
                else if (strtotime("today") >= $row["date"] && $row["date"] !== "unknown") {
                  echo "<img class = 'icon' alt = 'This task is late' src = 'static/warning.avif' title = 'This task is late'></img>
                  <span class = 'taskCard__date' style = 'color: #f62626'><b>" . $formattedDate . "</b></span>
                  <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";

                }
                else {
                  echo "<img class = 'icon' src = 'static/clock.avif'></img>
                  <span class = 'taskCard__date'><b>" . $formattedDate . "</b></span>
                  <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";
                }
              }


            else {

                if ((strtotime("tomorrow") >= $row["date"] || ($workweekMode == 1 && in_array(date("N"), array(5, 6, 7)) && date("N", $row["date"]) == 1 && (date("z", $row["date"]) - date("z")) < 5)) && strtotime("today") <= $row["date"] && $row["date"] !== "unknown") {
                  echo "<img class = 'icon' alt = 'This task is due soon' src = 'static/redclock.avif' title = 'This task is due soon'></img>
                  <span class = 'taskCard__date' style = 'color: #f62626'><b>" . $date . "</b></span>
                  <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";
                }
                else if (strtotime("today") >= $row["date"] && $row["date"] !== "unknown") {
                  echo "<img class = 'icon' alt = 'This task is late' src = 'static/warning.avif' title = 'This task is late'></img>
                  <span class = 'taskCard__date' style = 'color: #f62626'><b>" . $date . "</b></span>
                  <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";

                }
                else {
                  echo "<img class = 'icon' src = 'static/clock.avif'></img>
                  <span class = 'taskCard__date'><b>" . $date . "</b></span>
                  <hr><section style = 'max-height: 300px; text-align: left; overflow-y: auto; word-break: break-word'>" . $more . "</section>";
                }
              }

            echo "<div class = 'optionsWrap'>
            <a href = 'process.php?delete&deleteInstance&id=" . $row["id"] . "&token=" . $_SESSION["userToken"] . "' style = 'color: #1BAF5B; display: block;'>Finished</a>
            <a href = 'edittask.php?template=" . $row["template"] . "' style = 'color: #5294e2; display: block;'>Edit</a>
            </div>
            </div>";
              }
      }

  ?>

  <!-- delete btn:<a href = 'Delete' style = 'color: #f62626; display: block;'>Delete</a>
-->

</div>
<!-- <a href = 'index.php?view=today' class = "viewSwitchLink">Today View</a> -->
  <section class = "moreTasksLinkWrap moreTasksLinkWrap--left">
    <a href = 'index.php?view=today' class="chip chip--bare">
      <span class="chip__text">Today view</span>
      <span class="material-icons" style="font-size: 1.3em;">arrow_forward</span>
    </a>
  </section>


<?php
?>
