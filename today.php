<?php
?>

<script defer>
const body = document.body;

function toggleTaskMenu(el, open) {
  let taskControls;
  if (open) {
    taskControls = el.parentNode.querySelector("div .taskControls");
  }
  else {
    taskControls = el.parentNode;
  }
  taskControls.classList.toggle("taskControls--visible"); /* display: grid */
}

function postponeAllOverdue(overdueIDs, token) {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", "process.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onload = () => {
    if (xhr.readyState === xhr.DONE && xhr.status === 200) {
      location.reload();
    }
  };
  xhr.send("postponeAllOverdue&ids=" + overdueIDs + "&token=" + token);

}



function provisionOverdueChip(numOverdue, modalMarkup, overdueIDs, token) {
  let tasks = (numOverdue == 1) ? "task" : "tasks";
  console.log(overdueIDs);
  let chipMarkup = `
    <button class="chip chip--alert" id = "overdueTasksChip">
      <span class="chip__text">${numOverdue} overdue ${tasks}</span>
      <span class="material-icons" style="font-size: 1.3em;">arrow_forward</span>
    </button>
    `;
  document.querySelector(".bigHeader__content__more").insertAdjacentHTML("beforeend", chipMarkup);

  let modalHeader = `
    <section class = "modal__header">
      <h2>${numOverdue} overdue tasks</h2>
      <button class = "material-icons" title = "Postpone all non-recurring tasks" onclick="postponeAllOverdue('${overdueIDs}', '${token}')">low_priority</button>
      <button class = "material-icons closeModal">close</button>
    </section>
    `;
    modalMarkup = `<section class = "modal__body">` + modalMarkup + `</section>`;
    
  document.querySelector("#overdueTasksChip").addEventListener("click", (e) => {
    new Modal(e.target, modalHeader + modalMarkup);
  });

  Mousetrap.bind('o', () => {
    new Modal(document.activeElement, modalHeader + modalMarkup);
  });
}

class Modal {
  static TRAP_CANDIDATES = `
    a, button, input, select, textarea, svg, area, details, summary,
    iframe, object, embed, 
    [tabindex], [contenteditable]
  `;
  constructor (triggerEl, modalContents) {
    this.triggerEl = triggerEl; // the element that triggered the modal OR the element focused when the modal was opened (if it was opened by a keyboard shortcut)

    this.modalWrap = document.createElement("section");
    this.modalWrap.classList.add("modalWrap");
    this.modalWrap.addEventListener("click", (event) => {
      this.close(event);
    });
    body.addEventListener("keydown", (event) => {
      if (event.key == "Escape") {
        this.close(0);
      }
    }, { once: true });
    document.body.appendChild(this.modalWrap);

    // make modal itself
    this.modal = document.createElement("section");
    this.modal.classList.add("modal"); 
    this.modal.ariaModal = "true";
    this.modalWrap.appendChild(this.modal);

    // messy but works
    this.modal.insertAdjacentHTML("beforeend", modalContents);

    // if it exists, find close button
    try {
      this.modal.querySelector(".closeModal").addEventListener("click", () => {
        this.close();
      });
    }
    catch(error) {
      console.error(error);
    }

    // from stackoverflow / execute <script> tags. To call some of the code here "legacy" would be a gross overstatement.

    // fade in animation
    this.modalWrap.classList.add('visible');

    // move focus & trap it
    this.trapFocus();
    this.modal.querySelector(Modal.TRAP_CANDIDATES).focus(); // focus first clickable *thing*

    body.classList.add("noscroll");

  }
  async close(event = false) {
    if (event != false && (this.modalWrap != event.target)) return; // if event triggered by click, make sure the click wasn't INSIDE the modal

    this.modalWrap.classList.remove("visible");
    this.modalWrap.classList.add("hide");
    body.classList.remove("noscroll");
    this.releaseFocus();
    await new Promise(r => setTimeout(r, 400)); // Give animation time - ok if it takes longer than expected 
    body.removeChild(this.modalWrap);
  }

  trapFocus() {
    this.trappedNodes = [...document.querySelectorAll(Modal.TRAP_CANDIDATES)]
      .filter(node => !this.modal.contains(node) && node.getAttribute('tabindex') !== '-1');
    this.trappedNodes.forEach(node => node.setAttribute('tabindex', '-1'));
  }

  releaseFocus() {
    this.trappedNodes.forEach(node => node.removeAttribute('tabindex'));
    // refocus button that opened modal
    this.triggerEl.focus();
  }

}

function getGreeting(date) {
  let hour = date.getHours();
  if (hour < 12) {
    return "morning";
  }
  else if (hour < 18) {
    return "afternoon";
  }
  else {
    return "evening";
  }

}

function getOrdinalModifier(day) {
  if (day > 3 && day < 21) return 'th';
  switch (day % 10) {
    case 1:  return "st";
    case 2:  return "nd";
    case 3:  return "rd";
    default: return "th";
  }
}

window.addEventListener('DOMContentLoaded', (event) => {
  var today = new Date();

  let greeting = `<h1>
    <span>Good ${getGreeting(today)}.</span> 
    <span>It's
    <b>${today.toLocaleString('en-US', { weekday: 'long' })}</b>, 
    ${today.toLocaleString('en-US', { month: 'long', day: 'numeric'}) + getOrdinalModifier(today.getDate())}.</span>
  </h1>`;
  // Outputs: Good [time of day]. It's [weekday], [month nth].
  // The <span>s allow splitting into two lines on larger screens
  // the locale is hardcoded because this layout (ordinal modifier-thing at end, bolded weekday, etc) doesn't work with other languages. Plus rest of app is only in English, so mixed languages would be... weird.

  document.querySelector(".bigHeader__content").insertAdjacentHTML("afterbegin", greeting);
});


// yikes this needs its own file -- or multiple files!
</script>

<section class = "bigHeader bigHeader--todayView">
  <section class = "bigHeader__content">
    <section class = "bigHeader__content__more">
      <!-- what ti do with this???? -->
    </section>
    <?php
      if (isset($_GET["message"])) {
        echo "
        <section class = 'bigHeader__content__more'>
          <section class = 'bigHeader__content__message'>
            <span class='material-icons'>error_outline</span>
            <span class = 'bigHeader__content__subtitle'>" . htmlentities($_GET['message']) . "</span>
          </section>
        </section>";
      }
    ?>

  </section>
</section>

<section class = "normalWidthWrapper">
  <?php

    $userID = $_SESSION["userID"];

    $today = strtotime("today");

    $sqlSettings = "SELECT workweekMode, overdueExpand FROM settings WHERE user = '$userID' LIMIT 1";
    $resultSettings = mysqli_query($conn, $sqlSettings);
    if (mysqli_num_rows($resultSettings) == 1) {
      while($rowSettings = mysqli_fetch_assoc($resultSettings)) {
        $workweekMode = $rowSettings["workweekMode"];
        $overdueExpand = $rowSettings["overdueExpand"];
      }
    }
    // get all tasks that are active.
    $sql = "SELECT * FROM tasks WHERE user = '$userID' AND startDate <= '$today' ORDER BY date asc, priority desc, id desc";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
      $categories = [
        "Overdue" => [[], True],
        "Due today" => [[], False],
        "Due tomorrow" => [[], False],
        "Due Sunday" => [[], False], /* in practice, only shown on Friday with workweek mode. Shows tasks due Sunday so they aren't relegated below Monday's tasks. */
        "Due Monday" => [[], False],
        "Due soon" => [[], True],
        "Next 10 days" => [[], True], /* Not in love with this name since really it's only day ~3-10, but alternatives were worse: "Upcoming" - since upcoming implies inactive - but oh well. Maybe "up next", but same issue */
        "Keep in mind" => [[], True] /* due later but still active */
      ];
      $overdueIDs = [];
      while($row = mysqli_fetch_assoc($result)) {
          // only show active tasks
          if ($row["date"] == "unknown") {
            $dite = $row["date"];
          }
          else {
            $dite = date("m/d/Y", $row["date"]);
            $timestamp = $row["date"];
          }
          $badges = False;
          $messages = False;
        
          if ($row["freq"] > 0) {
            if ($row["endDate"] > strtotime("+" . $row["freq"] . " days", $row["date"])) { // if this this isn't the last occurrence
              $badges .= "<span class = 'material-icons badge' title = 'Recurring task'>loop</span>";
              $messages = "<span class = 'todayViewTaskMessages'>Repeats " . date("m/d/Y", strtotime("+" . $row["freq"] . " days", $row["date"])) . "</span>";
            }
            else { // if this is the last occurance
              $badges .= "<span class = 'material-icons badge badge--lastOccurance' title = 'Recurring task; last occurance'>loop</span>";
              $messages = "<span class = 'todayViewTaskMessages todayViewTaskMessages--urgent'>Last occurance</span>";
            }
          }
          if ($row["priority"] == 2) {
            $badges .= "<span class = 'material-icons badge' title = 'High priority task'>flag</span>";
          }
          $soonStyle = (
            (strtotime("tomorrow") >= $row["date"]
             && $row["date"] !== "unknown")
              ? "todayViewTaskDate--soon"
              : False);


          $postponeTaskTitle = (($row["date"] >= strtotime("today"))
            ? "Postpone task one day"
            : "Postpone task to today"
          );


          $overflowMenu = (($row["freq"] == 0)
            ? "<div style = 'position: relative;' class = 'controlsWrap'>
                <button class = 'openTaskControls' onclick='return toggleTaskMenu(this, true)'><span class='material-icons'>more_vert</span></button>
                <div class = 'taskControls taskControls--4col tasktaskControls--hidden'>
                  <a href = 'process.php?postpone&id=" . $row["id"] . "&token=" . $_SESSION["userToken"] . "' title = '" . $postponeTaskTitle . "'><span class='material-icons'>low_priority</span></a>
                  <a href = 'edittask.php?template=" . $row["template"] . "' title = 'Edit task'><span class='material-icons'>create</span></a>
                  <a href = 'process.php?delete&deleteInstance&id=" . $row["id"] . "&token=" . $_SESSION["userToken"] . "' style = 'color: #1BAF5B;' title = 'Finish task'><span class='material-icons'>check</span></a>
                  <button onclick='return toggleTaskMenu(this, false)'><span class='material-icons'>close</span></button>
                </div>
              </div>"
            : "<div style = 'position: relative;' class = 'controlsWrap'>
                <button class = 'openTaskControls' onclick='return toggleTaskMenu(this, true)'><span class='material-icons'>more_vert</span></button>
                <div class = 'taskControls taskControls--4col taskControls--hidden'>
                  <a href = 'process.php?split&id=" . $row["id"] . "&token=" . $_SESSION["userToken"] . "' title = 'Split this instance into its own task'><span class='material-icons'>alt_route</span></a>
                  <a href = 'edittask.php?template=" . $row["template"] . "' title = 'Edit task'><span class='material-icons'>create</span></a>
                  <a href = 'process.php?delete&deleteInstance&id=" . $row["id"] . "&token=" . $_SESSION["userToken"] . "' style = 'color: #1BAF5B;' title = 'Finish task'><span class='material-icons'>check</span></a>
                  <button onclick='return toggleTaskMenu(this, false)'><span class='material-icons'>close</span></button>
                </div>
              </div>"
          );

          // Semifuzzy date generator -- yesterday, day of week if within next week, or (Day, Month Num)
          // $dite is m/d/y
          if (date("m/d/Y", strtotime("yesterday")) == $dite) {
            $date = "Yesterday";
          }
          else if (date("m/d/Y", strtotime("today")) < $dite && date("m/d/Y", strtotime("+7 days")) > $dite) {
            $date = date("l", $timestamp);
          }
          else {
            $date = date("D, M jS", $timestamp);
          }

          $output = [
            "<div class = 'taskTodayName'>
              <a href = 'edittask.php?template=" . $row["template"] . "'>" . htmlentities($row["title"]) . "</a>
            </div>",

            $messages,

            "<span class = 'todayViewTaskDate " . $soonStyle . "'>" . $date . "</span>",

            $overflowMenu,

            $badges
          ];
          if (strtotime("yesterday") >= $row["date"]) {
            array_push($categories["Overdue"][0], $output);
            array_push($overdueIDs, $row["id"]);
          }
          else if (strtotime("today") >= $row["date"]) {
            array_push($categories["Due today"][0], $output);
          }
          else if (strtotime("tomorrow") >= $row["date"]) {
            array_push($categories["Due tomorrow"][0], $output);
          }

          // workweek mode only
          else if (($workweekMode == 1 && in_array(date("N"), array(5)) && date("N", $row["date"]) == 7 && (date("z", $row["date"]) - date("z")) < 3)) {
            array_push($categories["Due Sunday"][0], $output);
          }
          // workweek mode only
          else if (
            ($workweekMode == 1 && in_array(date("N"), array(5, 6)) // if today is Friday or Saturday
            && date("N", $row["date"]) == 1                        // and the task is due Monday
            && (strtotime("5 days") >= $row["date"]))) {          // and the task is within the next 5 days
            array_push($categories["Due Monday"][0], $output);
          }

          else if (strtotime("3 days") >= $row["date"] || (strtotime("4 days") >= $row["date"] && $row["priority"] == 2)) {
            array_push($categories["Due soon"][0], $output);
          }
          else if (strtotime("10 days") >= $row["date"]) {
            array_push($categories["Next 10 days"][0], $output);
          }
          else {
            array_push($categories["Keep in mind"][0], $output);
          }

      }
      echo "
        <a href = 'newtask.php' class = 'globalBtn' title = 'Create new task'>
          <span class = 'material-icons'>add</span>
        </a>
      ";
      // output categories as cards....
      $overdueTaskCount = 0;
      $overdueMarkup = "";
      foreach ($categories as $name => $category) {
        if (sizeof($category[0]) == 0) {
          continue;
        }
        if ($name !== "Overdue") {
          echo "
            <h2 class = 'todayViewSectionTitle'>" . $name . "</h2>
            <div class = 'item taskTodayWrap'>
          ";
        }

        $categoryMarkup = "";
        // put every task's markup into one variable per category (easy export to js for overdue)
        foreach ($category[0] as $task) {
          if ($category[1] == True) {
            if ($task[1] != False) { // messages
              $categoryMarkup .= "<div class = 'taskTodayRow taskTodayRow--withDate taskTodayRow--withDate--withMessages'>";
            }
            else {
              $categoryMarkup .= "<div class = 'taskTodayRow taskTodayRow--withDate'>";
            }
          }
          else {
            if ($task[1] != False) { // messages
              $categoryMarkup .= "<div class = 'taskTodayRow taskTodayRow--withMessages'>";
            }
            else {
              $categoryMarkup .= "<div class = 'taskTodayRow'>";
            }
          }
          if ($name == "Overdue") {
            $overdueTaskCount += 1;
          }
          $categoryMarkup .= $task[0];
          if ($category[1] == True) {
            // DO display date
            $categoryMarkup .= $task[2];
          }
          else {
            // DON'T display date (but still show badges)
          }
          if ($task[4] != False) {
            // display badges
            $categoryMarkup .= "<section class = 'todayViewTaskBadges'>" . $task[4] . "</section>";
          }
          if ($task[1] != False) {
            $categoryMarkup .= $task[1];
          }
          $categoryMarkup .= $task[3];
          $categoryMarkup .= "</div>";
        }
        if ($name == "Overdue") {
          $overdueMarkup = $categoryMarkup;
        }
        else {
          echo $categoryMarkup;
          echo "</div>";
        }
      }
      if ($overdueTaskCount > 0 && $overdueTaskCount < mysqli_num_rows($result)) {
        // Show number of overdue tasks & provision modal
        ?>
          <script>provisionOverdueChip(<?php echo $overdueTaskCount; ?>, <?php echo(json_encode($overdueMarkup)); ?>, <?php echo(json_encode($overdueIDs)) ?>, '<?php echo $_SESSION["userToken"] ?>')</script>
        <?php
      }
      else if ($overdueTaskCount > 0) {
        // ONLY overdue tasks, so don't put them behind a modal + add back wrapper/heading
        echo "<h2 class = 'todayViewSectionTitle'>" . $overdueTaskCount . " overdue tasks</h2>
              <div class = 'item taskTodayWrap'> " . $overdueMarkup . "</div>";
      }


    }
    else {
      ?>
      <div class = "item taskTodayWrap">
        <section class = 'noTasksToday'>
          <h1>🎉 You're all caught up!</h1>
          <p>New tasks will appear here when they need your attention.</p>
          <div class = 'btnsWrap'>
            <a href = 'newtask.php' class = 'inputBtn'>New Task</a>
            <a href = 'index.php?view=everything' class = 'inputBtn'>All Tasks</a>
          </div>
        </section>
      <?php
    }
    ?>


  </div>
  <section class = "moreTasksLinkWrap moreTasksLinkWrap--right">
    <a href = 'index.php?view=everything' class="chip chip--bare">
      <span class="chip__text">See all tasks</span>
      <span class="material-icons" style="font-size: 1.3em;">arrow_forward</span>
    </a>
  </section>

</section>

<?php
?>
